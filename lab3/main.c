#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>


size_t strlength(const char * s)
{
	size_t count = 0;
	while (*(s+count) != '\0') ++count;	
	return count;
}



#if 1

char * insert(char * s, const char * s1, size_t n)
{
	if (!s || !s1) return s;

	size_t len_s  = strlength(s);
	size_t len_s1 = strlength(s1);

	if (!len_s1 || n > len_s) return s;
	
	// copy (n...) to (n + len_s1...)
	for (size_t i = len_s; i + 1 > n; --i)
	{
		s[i + len_s1] = s[i];
	}

	// insert s1 into s
	for (size_t i = 0; i < len_s1; ++i)
	{
		s[i + n] = s1[i];
	}

	return s;
}

#else

char * insert(char * s, const char * s1, size_t n) 
{
	size_t len_s = strlength(s);
	size_t len_s1 = strlength(s1);

	if (!len_s1 || n > len_s) return s;

	memmove(s + n + len_s1, s + n, len_s - n + 1);
	memcpy(s + n, s1, len_s1);

	return s;
}

#endif


_Bool clean_stdin()
{
	while (getchar() != '\n');
	return true;
}


void get_integer(char * msg, int * v)
{
	char c;
	do
	{
		printf(msg);
	} while ((scanf("%d%c", v, &c) != 2 || c != '\n') && clean_stdin());
}



#define STR1_MAX 100
#define STR2_MAX 50

int main()
{
	char str1[STR1_MAX];
	char * str2 = (char *) malloc(STR2_MAX * sizeof(char));

	size_t pos = 0;

	// debug
	strcpy(str1, "My string!");
	insert(str1, "fucking ", 0);
	

	printf("Enter source string: ");
	fgets(str1, sizeof(str1), stdin);

	printf("Enter string for insert: ");
	fgets(str2, STR2_MAX, stdin);

	get_integer("Enter position for insert: ", &pos);
	

	if (pos >= strlength(str1))
	{
		printf("too large or too small position\n\n");
		return 1;
	}
	if (strlength(str1) + strlength(str2) > sizeof(str1))
	{
		printf("too big strings\n\n");
		return 2;
	}

	// delete \n at end
	str1[strlength(str1) - 1] = '\0';
	str2[strlength(str2) - 1] = '\0';

	// insert and show string
	insert(str1, str2, pos);
	printf("Result: %s\n\n", str1);


	return 0;
}