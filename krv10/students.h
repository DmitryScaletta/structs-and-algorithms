﻿#ifndef STUDENTS_H
#define STUDENTS_H
/*
Для заданной прикладной области разработать описание объектов этой области. 
Разработать процедуры, реализующие базовые операции над этими объектами, в том числе:
•	текстовый ввод-вывод (консольный и файловый);
•	присваивание;
•	задание константных значений;
•	сравнение (не менее 2-х типов).
Процедуры и описания данных должны составлять отдельный модуль (модуль типа данных).

Используя процедуры и описания модуля типа данных, разработать программу,
обеспечивающую ввод исходных данных из файла данных в память и хранение их в памяти
в виде упорядоченного разветвленного списка (для ветвления списка использовать естественную
классификацию объектов), сортировку списка и подсписков по алфавитному и по числовому параметру,
операции над этими списками.

Для каждой области перечислены параметры объекта. Среди параметров обязательно есть 
ключевое алфавитное поле (например, фамилия), которое идентифицирует объект, у 
каждого объекта имеется также одно или несколько числовых полей, по которым вероятны 
обращения к объекту. Набор характеристик может быть расширен и усложнен по усмотрению исполнителя.

10. Быт студентов
*/

#include <wchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <malloc.h>


#define Student struct Student_

struct Student_ {
	wchar_t * surname;		// фамилия студента
	wchar_t * name;			// имя
	wchar_t	* patronymic;	// отчество
	wchar_t * department;	// факультет
	size_t    grants;		// размер стипендии
	size_t    family_size;	// количество членов семьи
	Student * next;			// указатель на следующую запись
};

// поля записи
enum
{
	STUDENT_SURNAME,
	STUDENT_NAME,
	STUDENT_PATRONYMIC,
	STUDENT_DEPARTMENT,
	STUDENT_GRANTS,
	STUDENT_FAMILY_SIZE
};
// максимальные значения и длины полей (для строк с учетом '\0')
enum 
{
	STUDENT_SURNAME_MAX     = 100,
	STUDENT_NAME_MAX        = 100,
	STUDENT_PATRONYMIC_MAX  = 100,
	STUDENT_DEPARTMENT_MAX  = 50,
	STUDENT_GRANTS_MAX      = 1000000,
	STUDENT_FAMILY_SIZE_MAX = 100
};
// сортировка, порядок отображения и сравнение записей
enum 
{
	STUDENTS_SORT_BY_SURNAME,
	STUDENTS_SORT_BY_GRANTS,
	STUDENTS_ORDER_BY_ASC,
	STUDENTS_ORDER_BY_DESC,
	STUDENTS_COMPARE_BY_SURNAME,
	STUDENTS_COMPARE_BY_GRANTS
};

// разделитель между значениями при сохранении в файл
const char DELIM = ';';	




/*
	Подсчет количества цифр в числе
	\param[in] number входное число
	\returns   количество цифр в десятичном представлении числа
*/
size_t numeric_count(const int number)
{
	int _number = number;
	int count = (_number == 0) ? 1 : 0;
	while (_number != 0) 
	{
		count++;
		_number /= 10;
	}
	return count;
}



/**
	Преобразование строки в беззнаковое число
	\param[in]  str исходная строка
	\param[out] val указатель на переменную, в которую будет записан результат преобразования
	\returns	true в случае успешного преобразования, false при ошибке
*/
bool parse_size_t(const wchar_t * str, size_t * val)
{
	size_t _val = 0;
	wchar_t * temp;

	_val = wcstol(str, &temp, 0);

	if (*temp != '\0') return false;

	*val = _val;

	return true;
}



/**
	Выделение памяти для записи
	\param[in] surname_length    длина поля "фамилия"
	\param[in] name_length       длина поля "имя"
	\param[in] patronymic_length длина поля "отчество"
	\param[in] department_length длина поля "факультет"
	\returns   указатель на выделенную для записи память (в том числе на строковые поля) или нулевой указатель в случае ошибки
*/
Student * students_alloc(
	const size_t surname_length, 
	const size_t name_length,
	const size_t patronymic_length,
	const size_t department_length)
{
	// проверяем максимальную длину строк
	if (surname_length    >= STUDENT_SURNAME_MAX    ||
	    name_length       >= STUDENT_NAME_MAX       ||
	    patronymic_length >= STUDENT_PATRONYMIC_MAX ||
	    department_length >= STUDENT_DEPARTMENT_MAX) return NULL;

	Student * student = (Student *) malloc(sizeof(Student));

	student->surname    = (wchar_t *) malloc((surname_length    + 1) * sizeof(wchar_t));
	student->name       = (wchar_t *) malloc((name_length       + 1) * sizeof(wchar_t));
	student->patronymic = (wchar_t *) malloc((patronymic_length + 1) * sizeof(wchar_t));
	student->department = (wchar_t *) malloc((department_length + 1) * sizeof(wchar_t));

	return student;
}



/**
	Освобождения памяти записи
	\param[in] student указатель на запись для освобождения памяти
*/
void students_free(Student * student)
{
	if (!student) return;

	free(student->surname);
	free(student->name);
	free(student->patronymic);
	free(student->department);

	free(student);
}



/**
	Получение записи по фамилии студента
	\param[in] list    указатель на первую запись списка
	\param[in] surname фимилия студента
	\returns   первый попавшийся указатель на запись, с заданной фамилией, либо нулевой указатель, если фамилия не найдена
*/
Student * students_get(Student * list, const wchar_t * surname)
{
	if (!list) return NULL;

	Student * current = list;
	while (current != NULL)
	{
		if (!wcscmp(current->surname, surname)) return current;
		current = current->next;
	}
	
	return NULL;
}



/**
	Получение последней записи в списке
	\param[in] list    указатель на указатель на первую запись списка
	\returns   указатель на поледнюю запись списка
*/
Student * students_get_last(Student * list)
{
	if (!list) return NULL;

	Student * current = list;
	while (current->next != NULL)
	{
		current = current->next;
	}

	return current;
}



/**
	Форматированный вывод всех записей на экран
	\param[in] list  указатель на первую запись списка
	\param[in] split разбивать вывод по некоторому количеству записей (0 - не разбивать вывод)
*/
void students_show_all(const Student * list, const size_t split)
{
	const wchar_t * TOP_LINE    = L"┌────┬───────────────┬──────────────┬──────────────┬──────┬───────────┬───────┐\n";
	const wchar_t * HEADER      = L"│ #  │ Фамилия       │ Имя          │ Отчество     │ Фак. │ Стипедния │ Семья │\n";
	const wchar_t * MIDDLE_LINE = L"├────┼───────────────┼──────────────┼──────────────┼──────┼───────────┼───────┤\n";
	const wchar_t * ROW_LINE    = L"│ %-2.2d │ %-13.13ls │ %-12.12ls │ %-12.12ls │ %-4.4ls │ %-9d │ %-5d │\n";
	const wchar_t * BOTTOM_LINE = L"└────┴───────────────┴──────────────┴──────────────┴──────┴───────────┴───────┘\n";
	const wchar_t * NO_RECORDS  = L"Записей нет.\n\n";

	wprintf_s(TOP_LINE);
	wprintf_s(HEADER);

	if (!list)
	{
		wprintf_s(BOTTOM_LINE);
		wprintf_s(NO_RECORDS);
		return;
	}

	const Student * current = list;

	size_t count = 0;
	size_t split_count = 0;
	while (current != NULL)
	{
		if (split)
		{
			if (split_count == split)
			{
				wprintf_s(BOTTOM_LINE);
				system("pause");
				wprintf_s(TOP_LINE);
				wprintf_s(HEADER);
				split_count = 0;
			}
			++split_count;
		}

		wprintf_s(MIDDLE_LINE);
		wprintf_s(ROW_LINE, 
			++count, 
			current->surname, 
			current->name, 
			current->patronymic, 
			current->department, 
			current->grants, 
			current->family_size);
		current = current->next;
	}
	
	wprintf_s(BOTTOM_LINE);
	
	return;
}



/**
	Добавление записи в список
	\param[in] list указатель на указатель на первую запись списка
	\param[in] elem указатель на запись для добавления в список
	\returns   true в случае успешного добавления записи, false при ошибке
*/
bool students_push(Student ** list, Student * elem)
{
	if (!elem) return false;

	// получаем указатель на последнюю запись в списке
	Student * last_elem = (list) ? students_get_last(*list) : NULL;

	// если в списке нет ни одной записи
	if (!last_elem)
	{
		// присваиваем первой записи указатель на переданную запись
		*list = elem;
	}
	else
	{
		// присваиваем последней записи указатель на переданную запись
		last_elem->next = elem;
	}

	return true;
}



/**
	Добавление записи в список по всем полям
	\param[in] list         указатель на указатель на первую запись списка
	\param[in] surname      фамилия
	\param[in] name         имя
	\param[in] patronymic   отчество
	\param[in] department   факультет
	\param[in] grants       размер стипендии
	\param[in] family_size  число членов семьи
	\returns   true в случае успешного добавления записи, false при ошибке
*/
bool students_add(
	Student **      list,
	const wchar_t * surname,
	const wchar_t * name,
	const wchar_t * patronymic,
	const wchar_t * department,
	const size_t    grants,
	const size_t    family_size)
{
	// получаем длину строк
	size_t surname_length    = wcslen(surname);
	size_t name_length       = wcslen(name);
	size_t patronymic_length = wcslen(patronymic);
	size_t department_length = wcslen(department);
	
	// проверяем максимальные значения полей
	if (grants      >  STUDENT_GRANTS_MAX ||
		family_size >  STUDENT_FAMILY_SIZE_MAX) return false;

	// строки не могут содержать символ ';'
	if (wcsrchr(surname,    DELIM) ||
		wcsrchr(name,       DELIM) ||
		wcsrchr(patronymic, DELIM) ||
		wcsrchr(department, DELIM)) return false;



	// выделяем память для новой записи
	Student * elem = students_alloc(surname_length, name_length, patronymic_length, department_length);
	if (!elem) return false;

	// копируем строки
	wcscpy(elem->surname,    surname);
	wcscpy(elem->name,       name);
	wcscpy(elem->patronymic, patronymic);
	wcscpy(elem->department, department);

	// копирем числовые значения
	elem->grants      = grants;
	elem->family_size = family_size;

	// присваиваем нулевой указатель ссылке на следующую запись
	elem->next = NULL;
	


	// добавляем созданную запись в список записей
	return students_push(list, elem);
}



/**
	Удаление записи из списка
	\param[in] list    указатель на указатель на первую запись списка
	\param[in] surname фамилия студента для удаления
	\returns   true в случае успешного удаления, false если записи не существует
*/
bool students_delete(Student ** list, const wchar_t * surname)
{
	if (!list) return false;

	Student * current = *list; // текущая запись
	Student * prev    = NULL;  // предыдущая запись

	// цикл по списку
	while (current != NULL)
	{
		// если фаимлия совпадает
		if (!wcscmp(current->surname, surname))
		{
			// это не первая запись
			if (prev)
			{
				// меняем ссылку у предыдущей записи
				prev->next = current->next;
			}
			// это первая запись
			else
			{
				// меняем ссылку на начало списка
				*list = current->next;
			}

			// освобождаем память, выделенную для самомой удаляемой записи
			students_free(current);

			return true;
		}

		prev = current;
		current = current->next;
	}

	return false;
}



/**
	Удаление всех записей из списка
	\param[in] list    указатель на указатель на первую запись списка
	\returns   true в случае успешного удаления, false при ошибке
*/
bool students_clear(Student ** list)
{
	if (!list || !*list) return true;

	Student * current = *list;

	while (current)
	{
		Student * elem = current;
		current = current->next;
		students_free(elem);
	}

	*list = NULL;

	return true;
}



/**
	Изменение строкового поля в записи, находящейся в списке
	\param[in] elem      указатель на запись для изменения
	\param[in] field     поле для изменения
	\param[in] new_value новое значение для поля
	\returns   true в случае успешного изменения, false при ошибке
*/
bool students_change_string(Student * elem, const int field, const wchar_t * new_value)
{
	if (!elem || !new_value) return false;

	size_t length = wcslen(new_value);

	switch (field)
	{
		case STUDENT_SURNAME:
		{
			if (length >= STUDENT_SURNAME_MAX) return false;
			free(elem->surname);
			elem->surname = (wchar_t *) malloc((length + 1) * sizeof(wchar_t));;
			wcscpy(elem->surname, new_value);
		}
		break;

		case STUDENT_NAME:
		{
			if (length >= STUDENT_NAME_MAX) return false;
			free(elem->name);
			elem->name = (wchar_t *) malloc((length + 1) * sizeof(wchar_t));
			wcscpy(elem->name, new_value);
		}
		break;

		case STUDENT_PATRONYMIC:
		{
			if (length >= STUDENT_PATRONYMIC_MAX) return false;
			free(elem->patronymic);
			elem->patronymic = (wchar_t *) malloc((length + 1) * sizeof(wchar_t));
			wcscpy(elem->patronymic, new_value);
		}
		break;

		case STUDENT_DEPARTMENT:
		{
			if (length >= STUDENT_DEPARTMENT_MAX) return false;
			free(elem->department);
			elem->department = (wchar_t *) malloc((length + 1) * sizeof(wchar_t));
			wcscpy(elem->department, new_value);
		}
		break;

		default: return false;
	}

	return true;
}



/**
	Изменение числового поля в записи, находящейся в списке
	\param[in] elem      указатель на запись для изменения
	\param[in] field     поле для изменения
	\param[in] new_value новое значение для поля
	\returns   true в случае успешного изменения, false при ошибке
*/
bool students_change_number(Student * elem, const int field, const size_t new_value)
{
	if (!elem) return false;

	switch (field)
	{
		case STUDENT_GRANTS:
		{
			if (new_value > STUDENT_GRANTS_MAX) return false;
			elem->grants = new_value;
		}
		break;

		case STUDENT_FAMILY_SIZE:
		{
			if (new_value > STUDENT_FAMILY_SIZE_MAX) return false;
			elem->family_size = new_value;
		}
		break;

		default: return false;
	}

	return true;
}



/**
	Сравнение двух записей
	\param[in] elem1      указатель на первую запись для сравнения
	\param[in] elem2      указатель на вторую запись для сравнения
	\param[in] compare_by поле для сравнения
	\returns   0 - если записи равны, положительное число - если первая запись больше второй, отрицательное число - если первая запись меньше второй
*/
int students_compare(const Student * elem1, const Student * elem2, const int compare_by)
{
	switch (compare_by)
	{
		default:
		case STUDENTS_COMPARE_BY_SURNAME:
		{
			return wcscmp(elem1->surname, elem2->surname);
		}
		break;

		case STUDENTS_COMPARE_BY_GRANTS:
		{
			return (elem1->grants == elem2->grants) ? 0 : (elem1->grants > elem2->grants) ? 1 : -1;
		}
		break;
	}
}



/**
	Перестановка двух записей
	\param[in] prev1 указатель на указатель на запись, стоящую перед записью elem1
	\param[in] elem1 указатель на указатель на первую запись для перестановки
	\param[in] prev2 указатель на указатель на запись, стоящую перед записью elem2
	\param[in] elem2 указатель на указатель на вторую запись для перестановки
	\returns   true в случае успешной перестановки, false при ошибке
*/
bool students_swap(Student ** prev1, Student ** elem1, Student ** prev2, Student ** elem2)
{
	if (!elem1 || !elem2) return false;

	Student * tmp;

	// меняем сами записи
	tmp    = *elem1;
	*elem1 = *elem2; // e2
	*elem2 = tmp;    // e1

	// меняем ссылки предыдущим записям
	if (prev1) (*prev1)->next = *elem1; // e2
	if (prev2) (*prev2)->next = *elem2; // e1

	// меняем ссылки
	tmp            = (*elem1)->next;
	(*elem1)->next = (*elem2)->next; //  e1n
	(*elem2)->next = tmp;            //  e2n
	

	return true;
}



/**
	Сортировка списка записей
	\param[in] list     указатель на указатель на первую запись списка
	\param[in] sort_by  поле для сортировки
	\param[in] order_by порядок сортировки
	\returns   true в случае успешной сортировки, false при ошибке
*/
bool students_sort(Student ** list, const int sort_by, const int order_by)
{
	if (!list  || 
		!*list ||
		(sort_by  != STUDENTS_SORT_BY_SURNAME && sort_by  != STUDENTS_SORT_BY_GRANTS) ||
		(order_by != STUDENTS_ORDER_BY_ASC    && order_by != STUDENTS_ORDER_BY_DESC))
		return false;

	bool is_swaped = true;		// была ли хоть одна перестановка
	bool need_swap = false;		// нужна ли перестановка
	int  cmp       = 0;			// результат функции сравнения

	while (is_swaped)
	{
		Student ** prev = NULL;				// предыдущая запись
		Student ** curr = list;				// текущая запись
		Student ** next = &((*list)->next);	// следующая запись

		is_swaped = false;
		while (*next != NULL)
		{
			// получаем результат сравнения записей
			switch (sort_by)
			{
				case STUDENTS_SORT_BY_SURNAME: cmp = students_compare(*curr, *next, STUDENTS_COMPARE_BY_SURNAME);
				break;

				case STUDENTS_SORT_BY_GRANTS:  cmp = students_compare(*curr, *next, STUDENTS_COMPARE_BY_GRANTS);
				break;
			}

			// нужна ли перестновка
			need_swap = (order_by == STUDENTS_ORDER_BY_ASC) ? cmp > 0 : cmp < 0;

			// переставляем записи если нужно
			if (need_swap)
			{
				students_swap(prev, curr, NULL, next);
				is_swaped = true;

				prev = next;
				// curr = curr;
			}
			else
			{
				prev = curr;
				curr = next;
			}
			next = &((*curr)->next);
		}

	}


	return true;
}



/**
	Cохранение списка записей в файл
	\param[in] list     указатель на первую запись списка
	\param[in] filename полный путь к файлу
	\returns   количество загруженных запией, -1 при ошибке
*/
int students_save_to_file(const Student * list, const wchar_t * filename)
{
	// открываем файл
	FILE * file = _wfopen(filename, L"w,ccs=UTF-8");

	if (!file) return -1;

	// записываем каждую запись в файл
	int count = 0;
	const Student * current = list;
	while (current != NULL)
	{
		fwprintf_s(file, 
			L"%ls%c%ls%c%ls%c%ls%c%d%c%d\n",
			current->surname,
			DELIM,
			current->name,
			DELIM,
			current->patronymic,
			DELIM,
			current->department,
			DELIM,
			current->grants,
			DELIM,
			current->family_size);

		++count;

		current = current->next;
	}

	// закрываем файл
	fclose(file);

	return count;
}



/**
	Загрузка списка записей из файла
	\param[out] list     указатель на указатель на первую запись списка
	\param[in]  filename полный путь к файлу
	\returns    количество загруженных запией, -1 при ошибке
*/
int students_load_from_file(Student ** list, const wchar_t * filename)
{
	// открываем файл
	FILE * file = _wfopen(filename, L"r,ccs=UTF-8");

	if (!file) return -1;

	// удаляем все предыдущие записи в списке
	students_clear(list);

	size_t count = 0;	// количество добавленных записей из файла

	// максимально возможная длина строки в файле
	const size_t MAX_LINE = 
		STUDENT_SURNAME_MAX + 
		STUDENT_NAME_MAX + 
		STUDENT_PATRONYMIC_MAX + 
		STUDENT_DEPARTMENT_MAX + 
		numeric_count(STUDENT_GRANTS_MAX) + 
		numeric_count(STUDENT_FAMILY_SIZE_MAX) + 
		7; // ';' * 5 + '\n' + '\0'

	wchar_t * buffer = (wchar_t *) malloc(MAX_LINE * sizeof(wchar_t));

	wchar_t * pos = NULL;

	// читаем каждую строку в файле
	while (fgetws(buffer, MAX_LINE, file))
	{
		// ищем адрес начала и количество символов для каждого поля записи

		// surname
		size_t surname_start = 0;
		pos = wcschr(buffer + surname_start, DELIM); if (!pos) continue;
		size_t surname_count = pos - buffer - surname_start;
		if (surname_count >= STUDENT_SURNAME_MAX) continue;


		// name
		size_t name_start = surname_start + surname_count + 1;
		pos = wcschr(buffer + name_start, DELIM); if (!pos) continue;
		size_t name_count = pos - buffer - name_start;
		if (name_count >= STUDENT_NAME_MAX) continue;
			

		// patronymic
		size_t patronymic_start = name_start + name_count + 1;
		pos = wcschr(buffer + patronymic_start, DELIM); if (!pos) continue;
		size_t patronymic_count = pos - buffer - patronymic_start;
		if (patronymic_count >= STUDENT_PATRONYMIC_MAX) continue;
		

		// department
		size_t department_start = patronymic_start + patronymic_count + 1;
		pos = wcschr(buffer + department_start, DELIM); if (!pos) continue;
		size_t department_count = pos - buffer - department_start;
		if (department_count >= STUDENT_DEPARTMENT_MAX) continue;
		

		// grants
		size_t grants_start = department_start + department_count + 1;
		pos = wcschr(buffer + grants_start, DELIM); if (!pos) continue;
		size_t grants_count = pos - buffer - grants_start;
		
		wchar_t * grants_s = (wchar_t *) malloc((grants_count + 1) * sizeof(wchar_t));
		wcsncpy(grants_s, buffer + grants_start, grants_count);
		grants_s[grants_count] = '\0';

		size_t grants = 0;
		bool success_parsed;
		success_parsed = parse_size_t(grants_s, &grants);
		free(grants_s);
		if (!success_parsed || grants > STUDENT_GRANTS_MAX) continue;
		

		// family_size
		size_t family_size_start = grants_start + grants_count + 1;
		pos = wcschr(buffer + family_size_start, '\n'); if (!pos) continue;
		size_t family_size_count = pos - buffer - family_size_start;
		
		wchar_t * family_size_s = (wchar_t *) malloc((family_size_count + 1) * sizeof(wchar_t));
		wcsncpy(family_size_s, buffer + family_size_start, family_size_count);
		family_size_s[family_size_count] = '\0';

		size_t family_size = 0;
		success_parsed = parse_size_t(family_size_s, &family_size);
		free(family_size_s);
		if (!success_parsed || family_size > STUDENT_FAMILY_SIZE_MAX) continue;
		


		// выделяем память для новой записи
		Student * elem = students_alloc(surname_count, name_count, patronymic_count, department_count);

		// копируем строки
		wcsncpy(elem->surname,    buffer + surname_start, surname_count);
		wcsncpy(elem->name,       buffer + name_start, name_count);
		wcsncpy(elem->patronymic, buffer + patronymic_start, patronymic_count);
		wcsncpy(elem->department, buffer + department_start, department_count);

		elem->surname   [surname_count]    = '\0';
		elem->name      [name_count]       = '\0';
		elem->patronymic[patronymic_count] = '\0';
		elem->department[department_count] = '\0';

		// копирем числовые значения
		elem->grants      = grants;
		elem->family_size = family_size;

		// присваиваем нулевой указатель ссылке на следующую запись
		elem->next = NULL;


		// добавляем созданную запись в список записей
		students_push(list, elem);

		++count;	
	}

	// освобождаем буфер
	free(buffer);

	// закрываем файл
	fclose(file);

	return count;
}



#endif