﻿#ifndef UNIT_TESTS_H
#define UNIT_TESTS_H

#include "students.h"


#define TITLE(title)       wprintf_s(L"\n-------------------\n%ls\n-------------------\n\n", title)
#define TEST(title, value) wprintf_s(L"# %ls\n> %ls\n\n", title, (value) ? L"Success" : L"Failure")





void run_tests()
{
	Student * list = NULL;

	

	TITLE(L"students_add()");
	
	TEST(
		L"добавляем 1",
		students_add(&list, L"Иванов", L"Иван", L"Иванович", L"Мат", 550000, 6)
	);

	TEST(
		L"добавляем 2",
		students_add(&list, L"Петров", L"Петр", L"Петрович", L"Физ", 780000, 4)
	);

	TEST(
		L"добавляем 3",
		students_add(&list, L"Сидоров", L"Сидор", L"Сидорович", L"Бух", 358000, 3)
	);

	TEST(
		L"добавляем 4",
		students_add(&list, L"Семенов", L"Семен", L"Семенович", L"Мат", 560000, 5)
	);

	TEST(
		L"добавляем 5",
		students_add(&list, L"Витальев", L"Виталий", L"Витальевич", L"Фил", 659000, 8)
	);

	TEST(
		L"добавляем 5",
		students_add(&list, L"Олегов", L"Олег", L"Олегович", L"Физ", 413000, 2)
		);

	TEST(
		L"добавляем неправильный",
		students_add(&list, L"", L"", L"", L"", 10000000, 3) == false
	);




	TITLE(L"students_delete()");

	TEST(
		L"удаляем первый",
		students_delete(&list, L"Иванов") && list->grants == 780000
	);
	
	TEST(
		L"удаляем в середине",
		students_delete(&list, L"Сидоров")
	);

	TEST(
		L"удаляем последний",
		students_delete(&list, L"Олегов")
	);

	TEST(
		L"удаляем несуществующий",
		students_delete(&list, L"Несуществующий") == false
	);




	Student * s;

	TITLE(L"students_get()");

	s = students_get(list, L"Петров");
	TEST(
		L"Петров",
		s->grants == 780000
	);

	s = students_get(list, L"Несуществующий");
	TEST(
		L"Несуществующий",
		s == NULL
	);




	TITLE(L"students_get()");
	
	s = students_get_last(list);
	TEST(
		L"последний",
		s->grants == 659000
	);

	s = students_get_last(NULL);
	TEST(
		L"нет записей в списке",
		s == NULL
	);

	


	TITLE(L"students_change_string()");

	s = students_get(list, L"Петров");
	TEST(
		L"первая запись, фамилия",
		students_change_string(s, STUDENT_SURNAME, L"Петрова") && wcscmp(s->surname, L"Петрова") == 0
	);

	TEST(
		L"первая запись, имя",
		students_change_string(s, STUDENT_NAME, L"Имя") && wcscmp(s->name, L"Имя") == 0
	);

	s = students_get(list, L"Семенов");
	TEST(
		L"запись в середине, отчество",
		students_change_string(s, STUDENT_PATRONYMIC, L"Отчество") && wcscmp(s->patronymic, L"Отчество") == 0
	);

	s = students_get(list, L"Витальев");
	TEST(
		L"последняя запись, факультет",
		students_change_string(s, STUDENT_DEPARTMENT, L"фак") && wcscmp(s->department, L"фак") == 0
	);

	TEST(
		L"последняя запись, стипендия",
		students_change_number(s, STUDENT_GRANTS, 666666) && s->grants == 666666
	);

	TEST(
		L"последняя запись, семья",
		students_change_number(s, STUDENT_FAMILY_SIZE, 10) && s->family_size == 10
	);

	TEST(
		L"последняя запись, неправильное значение",
		(students_change_number(s, STUDENT_FAMILY_SIZE, 456) && s->family_size == 10) == false
	);




	TITLE(L"students_swap()");
	

	TEST(L"Петрова, Семенов, Витальев", true);
	students_show_all(list, 0);
	TEST(
		L"перавя и вторая записи",
		students_swap(NULL, &list, NULL, &(list->next)) && (wcscmp(list->surname, L"Семенов") == NULL && wcscmp((list->next)->surname, L"Петрова") == NULL)
	);

	TEST(L"Семенов, Петрова, Витальев", true);
	students_show_all(list, 0);
	TEST(
		L"перавя и последняя записи",
		students_swap(NULL, &list, &(list->next), &((list->next)->next)) && (wcscmp(list->surname, L"Витальев") == NULL && wcscmp(((list->next)->next)->surname, L"Семенов") == NULL)
	);

	TEST(L"Витальев, Петрова, Семенов", true);
	students_show_all(list, 0);
	TEST(
		L"вторая и последняя записи",
		students_swap(&list, &(list->next), NULL, &((list->next)->next)) && (wcscmp((list->next)->surname, L"Семенов") == NULL && wcscmp(((list->next)->next)->surname, L"Петрова") == NULL)
	);

	TEST(L"Витальев, Семенов, Петрова", true);
	students_show_all(list, 0);

}

#endif