﻿#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <malloc.h>
#include <conio.h>		// _getch()
#include <fcntl.h>		// _O_U8TEXT
#include <Windows.h>	// SetConsoleTitle()

#include "students.h"


#define PAUSE         system("pause")		// пауза
#define CLS           system("cls")			// очистить экран
#define GETCHAR(x)    x = _getch()			// запросить ввод
#define BACKUP_FILE   L"data.txt"			// файл для резервного копирования
#define IF_LIST_EMPTY if (!list) { wprintf_s(L"Записей нет.\n\n"); break; }

enum { CHAR_0 = 48, CHAR_1, CHAR_2, CHAR_3, CHAR_4, CHAR_5, CHAR_6, CHAR_7, CHAR_8, CHAR_9 };



void win1251_to_utf8(const wchar_t * s, wchar_t * ws, const size_t size)
{
	/*
	---------------------------------
	|     | win1251   | Unicode     |
	---------------------------------
	| A-я | 0xC0-0xFF | 0x410-0x44F | diff = 0x350
	| Ё   | 0xA8      | 0x401       |
	| ё   | 0xB8      | 0x451       |
	---------------------------------
	*/
	
	for (size_t i = 0; i < size; ++i)
	{
		if (s[i] >= 0xC0 && s[i] <= 0xFF)
		{
			ws[i] = s[i] + 0x350;
		}
		else if (s[i] == 0xA8)
		{
			ws[i] = 0x401;
		}
		else if (s[i] == 0xB8)
		{
			ws[i] = 0x451;
		}
		else
		{
			ws[i] = s[i];
		}
	}
}


// очистка буфера ввода
bool clean_stdin()
{
	while (getchar() != '\n');
	return true;
}


// чтение чиста с клавиатуры
bool get_size_t(size_t * val)
{
	char c;
	if (scanf("%d%c", val, &c) != 2 || c != '\n')
	{
		clean_stdin();
		return false;
	}
	return true;
}


// чтение строки с клавиатуры
size_t get_string(wchar_t * str, const size_t max_size)
{
	size_t i;
	int c = 0;

	wchar_t * buffer = (wchar_t *) malloc(max_size * sizeof(wchar_t));

	for (i = 0; i < max_size - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		buffer[i] = c;

	if (c == '\n')
	{
		buffer[i] = c;
		++i;
	}
	buffer[--i] = '\0';

	win1251_to_utf8(buffer, str, max_size);

	free(buffer);

	return i;
}


// получение поля студента с клавиатуры
bool get_student_value(const int field, wchar_t * string_value, size_t * size_t_value, const size_t max_value)
{
	const wchar_t * EMPTY_VALUE     = L"Значение не может быть пустым.\n";
	const wchar_t * FORBIDDEN_CHAR  = L"Значение не может содержать символ: %c.\n";
	const wchar_t * TOO_BIG_VALUE   = L"Слишком большое значение.\n";
	const wchar_t * FORBIDDEN_VALUE = L"Недопустимое значение.\n";

	bool need_loop = true;
	while (need_loop)
	{
		switch (field)
		{
			case STUDENT_SURNAME:     wprintf_s(L"Введите фамилию: ");                 break;
			case STUDENT_NAME:        wprintf_s(L"Введите имя: ");                     break;
			case STUDENT_PATRONYMIC:  wprintf_s(L"Введите отчество: ");                break;
			case STUDENT_DEPARTMENT:  wprintf_s(L"Введите факультет: ");               break;
			case STUDENT_GRANTS:      wprintf_s(L"Введите стипендию: ");               break;
			case STUDENT_FAMILY_SIZE: wprintf_s(L"Введите количество членов семьи: "); break;

			default: return false;
		}

		switch (field)
		{
			case STUDENT_SURNAME:
			case STUDENT_NAME:
			case STUDENT_PATRONYMIC:
			case STUDENT_DEPARTMENT:
			{
				if (!string_value) return false;
				get_string(string_value, max_value);
				if (wcslen(string_value) == 0)    { wprintf_s(EMPTY_VALUE);           continue; }
				if (wcsrchr(string_value, DELIM)) { wprintf_s(FORBIDDEN_CHAR, DELIM); continue; }
				need_loop = false;
			}
			break;

			case STUDENT_GRANTS:
			case STUDENT_FAMILY_SIZE:
			{
				if (!size_t_value) return false;
				if (!get_size_t(size_t_value)) { wprintf_s(FORBIDDEN_VALUE); continue; }
				if (*size_t_value > max_value) { wprintf_s(TOO_BIG_VALUE);   continue; }
				need_loop = false;
			}
			break;
		}
	}

	return true;
}


// получение записи
bool get_record(Student * list, Student ** elem)
{
	if (!list || !elem) return false;

	wchar_t surname[STUDENT_SURNAME_MAX];
	get_student_value(STUDENT_SURNAME, surname, NULL, STUDENT_SURNAME_MAX);

	*elem = students_get(list, surname);
	if (!*elem)
	{
		wprintf_s(L"Студента с такой фамилией не найдено.\n");
		PAUSE;
		return false;
	}

	return true;
}


// добавление записи
bool add_record(Student ** list)
{
	wchar_t surname   [STUDENT_SURNAME_MAX];
	wchar_t name      [STUDENT_NAME_MAX];
	wchar_t patronymic[STUDENT_PATRONYMIC_MAX];
	wchar_t department[STUDENT_DEPARTMENT_MAX];

	size_t grants      = 0;
	size_t family_size = 0;

	get_student_value(STUDENT_SURNAME,    surname,    NULL, STUDENT_SURNAME_MAX);
	get_student_value(STUDENT_NAME,       name,       NULL, STUDENT_NAME_MAX);
	get_student_value(STUDENT_PATRONYMIC, patronymic, NULL, STUDENT_PATRONYMIC_MAX);
	get_student_value(STUDENT_DEPARTMENT, department, NULL, STUDENT_DEPARTMENT_MAX);

	get_student_value(STUDENT_GRANTS,      NULL, &grants,      STUDENT_GRANTS_MAX);
	get_student_value(STUDENT_FAMILY_SIZE, NULL, &family_size, STUDENT_FAMILY_SIZE_MAX);

	if (!students_add(list, surname, name, patronymic, department, grants, family_size)) return false;

	return true;
}


// изменение поля в записи
bool change_field(Student * elem, const int field)
{
	wchar_t * string_value = NULL;
	size_t    max          = 0;
	size_t    size_t_value = 0;
	bool      res          = false;

	switch (field)
	{
		case STUDENT_SURNAME:     max = STUDENT_SURNAME_MAX;     break;
		case STUDENT_NAME:        max = STUDENT_NAME_MAX;        break;
		case STUDENT_PATRONYMIC:  max = STUDENT_PATRONYMIC_MAX;  break;
		case STUDENT_DEPARTMENT:  max = STUDENT_DEPARTMENT_MAX;  break;
		case STUDENT_GRANTS:      max = STUDENT_GRANTS_MAX;      break;
		case STUDENT_FAMILY_SIZE: max = STUDENT_FAMILY_SIZE_MAX; break;

		default: return false;
	}

	switch (field)
	{
		case STUDENT_SURNAME:    
		case STUDENT_NAME:
		case STUDENT_PATRONYMIC:
		case STUDENT_DEPARTMENT:
		{
			string_value = (wchar_t *) malloc((max + 1) * sizeof(wchar_t));
			get_student_value(field, string_value, NULL, max);
			res = students_change_string(elem, field, string_value);
		}
		break;

		case STUDENT_GRANTS:
		case STUDENT_FAMILY_SIZE:
		{
			get_student_value(field, NULL, &size_t_value, max);
			res = students_change_number(elem, field, size_t_value);
		}
		break;

		default: return false;
	}

	if (res)
	{
		wprintf_s(L"Запись успешно изменена.\n");
	}
	else
	{
		wprintf_s(L"Ошибка. Не удалось изменить запись.\n");
	}

	return res;
}






int main()
{
	// console things
	_setmode(_fileno(stdout), _O_U8TEXT);
	SetConsoleCP(1251);
	//SetConsoleOutputCP(1251);
	SetConsoleTitle(L"Students");



	const wchar_t * MAIN_MENU =
		L"┌─────────────────────────────────────────────────────────────────────────────┐\n"
		L"│ Students (Singly Linked List)                    Created by: DmitryScaletta │\n"
		L"└─────────────────────────────────────────────────────────────────────────────┘\n"
		L" 1. Добавить запись\n"
		L" 2. Удалить запись\n"
		L" 3. Изменить запись\n"
		L" 4. Просмотреть запись\n"
		L" 5. Просмотреть все записи\n"
		L" 6. Сравнить записи\n"
		L" 7. Отсортировать записи\n"
		L" 8. Резервное копирование\n"
		L" ---------\n"
		L" 0. Выход\n"
		L" ---------\n";

	const wchar_t * CHANGE_RECORD =
		L" Изменение записи\n"
		L" 1. Изменить все поля\n"
		L" 2. Изменить фамилию\n"
		L" 3. Изменить имя\n"
		L" 4. Изменить отчество\n"
		L" 5. Изменить факультет\n"
		L" 6. Изменить стипендию\n"
		L" 7. Изменить количество членов семьи\n"
		L" ---------\n"
		L" 0. Назад\n"
		L" ---------\n";

	const wchar_t * COMPARE_RECORDS = 
		L" Сравнение записей\n"
		L" 1. Сравнить по фамилии\n"
		L" 2. Сравнить по стипендии\n"
		L" ---------\n"
		L" 0. Назад\n"
		L" ---------\n";

	const wchar_t * SORT_RECORDS =
		L" Сортировка записей\n"
		L" 1. По фамилии (по возростанию)\n"
		L" 2. По фамилии (по убыванию)\n"
		L" 3. По стипендии (по возростанию)\n"
		L" 4. По стипендии (по убыванию)\n"
		L" ---------\n"
		L" 0. Назад\n"
		L" ---------\n";

	const wchar_t * BACKUP =
		L" Резервное копирование.\n"
		L" 1. Сохранить записи в файл\n"
		L" 2. Загрузить записи из файла\n"
		L" ---------\n"
		L" 0. Назад\n"
		L" ---------\n";

	const wchar_t * EXIT_QUESTION =
		L" Вы действительно хотите выйти?.\n"
		L" 1. Да\n"
		L" 2. Нет\n";



	// указатель на начало списка
	Student * list = NULL;



	// главное меню
	while (true)
	{
		CLS;
		wprintf_s(MAIN_MENU);

		int x;
		int x1;
		GETCHAR(x);
		CLS;

		switch (x)
		{
			// Выход
			case CHAR_0:
			{
				wprintf_s(EXIT_QUESTION);
				GETCHAR(x1);
				switch (x1)
				{
					case CHAR_1: x = -1; break;
					default:     continue;
				}
			}
			break;


			// Добавить запись
			case CHAR_1:
			{
				wprintf_s(L"Добавление записи.\n");

				if (add_record(&list))
				{
					wprintf_s(L"Запись успешно добавлена.\n");
				}
				else
				{
					wprintf_s(L"Ошибка. Не удалось добавить запись.\n");
				}
			}
			break;


			// Удалить запись
			case CHAR_2:
			{
				wprintf_s(L"Удаление записи.\n");

				IF_LIST_EMPTY;
				

				wchar_t surname[STUDENT_SURNAME_MAX];
				get_student_value(STUDENT_SURNAME, surname, NULL, STUDENT_SURNAME_MAX);

				if (students_delete(&list, surname))
				{
					wprintf_s(L"Запись успешно удалена.\n");
				}
				else
				{
					wprintf_s(L"Ошибка. Не удалось удалить запись.\n");
				}
			}
			break;


			// Изменить запись
			case CHAR_3:
			{
				wprintf_s(L"Изменение записи.\n");

				IF_LIST_EMPTY;
				
				Student * elem = NULL;
				if (!get_record(list, &elem)) continue;

				CLS;
				wprintf_s(CHANGE_RECORD);
				GETCHAR(x1);
				CLS;
				switch (x1)
				{
					// назад
					case CHAR_0: break;

					// Изменить все поля
					case CHAR_1:
					{
						change_field(elem, STUDENT_SURNAME);
						change_field(elem, STUDENT_NAME);
						change_field(elem, STUDENT_PATRONYMIC);
						change_field(elem, STUDENT_DEPARTMENT);
						change_field(elem, STUDENT_GRANTS);
						change_field(elem, STUDENT_FAMILY_SIZE);
					}
					break;
					
					// изменить отдельные поля
					case CHAR_2: change_field(elem, STUDENT_SURNAME);     break;
					case CHAR_3: change_field(elem, STUDENT_NAME);        break;
					case CHAR_4: change_field(elem, STUDENT_PATRONYMIC);  break;
					case CHAR_5: change_field(elem, STUDENT_DEPARTMENT);  break;
					case CHAR_6: change_field(elem, STUDENT_GRANTS);      break;
					case CHAR_7: change_field(elem, STUDENT_FAMILY_SIZE); break;

					default: break;
				}
			}
			break;


			// Просмотреть запись
			case CHAR_4:
			{
				wprintf_s(L"Просмотр записи.\n");

				IF_LIST_EMPTY;
				
				Student * elem = NULL;
				if (!get_record(list, &elem)) continue;

				wprintf_s(
					L"\nФамилия:\t%ls\n"
					L"Имя:\t\t%ls\n"
					L"Отчество:\t%ls\n"
					L"Факультет:\t%ls\n"
					L"Стипендия:\t%d\n"
					L"Семья:\t\t%d\n\n",
					elem->surname,
					elem->name,
					elem->patronymic,
					elem->department,
					elem->grants,
					elem->family_size);
			}
			break;


			// Просмотреть все записи
			case CHAR_5:
			{
				students_show_all(list, 10);
			}
			break;


			// Сравнить записи
			case CHAR_6:
			{
				IF_LIST_EMPTY;

				wprintf_s(COMPARE_RECORDS);
				GETCHAR(x1);
				CLS;

				if (x1 != CHAR_1 && x1 != CHAR_2) break;

				int compare_by = 0;
				switch (x1)
				{
					case CHAR_1: 
						wprintf_s(L"Сравнение записей по фамилии\n");
						compare_by = STUDENTS_COMPARE_BY_SURNAME; 
						break;
					case CHAR_2: 
						wprintf_s(L"Сравнение записей по стипендии\n"); 
						compare_by = STUDENTS_COMPARE_BY_GRANTS;
						break;
				}

				wprintf_s(L"Первая запись\n");
				Student * elem1 = NULL;
				if (!get_record(list, &elem1)) continue;

				wprintf_s(L"Вторая запись\n");
				Student * elem2 = NULL;
				if (!get_record(list, &elem2)) continue;

				int res = 0;
				res = students_compare(elem1, elem2, compare_by);

				if (res > 0)
				{
					wprintf_s(L"Первая запись больше второй.\n");
				}
				else if (res < 0)
				{
					wprintf_s(L"Первая запись меньше второй.\n");
				}
				else
				{
					wprintf_s(L"Записи равны.\n"); 
				}
			}
			break;


			// Отсортировать записи
			case CHAR_7:
			{
				IF_LIST_EMPTY;

				wprintf_s(SORT_RECORDS);
				GETCHAR(x1);
				CLS;

				bool rc = false;
				switch (x1)
				{
					case CHAR_1: rc = students_sort(&list, STUDENTS_SORT_BY_SURNAME, STUDENTS_ORDER_BY_ASC);  break;
					case CHAR_2: rc = students_sort(&list, STUDENTS_SORT_BY_SURNAME, STUDENTS_ORDER_BY_DESC); break;
					case CHAR_3: rc = students_sort(&list, STUDENTS_SORT_BY_GRANTS,  STUDENTS_ORDER_BY_ASC);  break;
					case CHAR_4: rc = students_sort(&list, STUDENTS_SORT_BY_GRANTS,  STUDENTS_ORDER_BY_DESC); break;
					default: break;
				}

				if (rc)
				{
					wprintf_s(L"Записи успешно отсортированы.\n");
				}
				else
				{
					wprintf_s(L"Ошибка. Не удалось отсортировать записи.\n");
				}
			}
			break;


			// Резервное копирование
			case CHAR_8:
			{
				wprintf_s(BACKUP);
				GETCHAR(x1);
				CLS;
				switch (x1)
				{
					// сохранить в файл
					case CHAR_1:
					{
						IF_LIST_EMPTY;

						int rc = students_save_to_file(list, BACKUP_FILE);
						if (rc != -1)
						{
							wprintf_s(L"Записи успешно сохранены в файл.\n");
							wprintf_s(L"Количество сохраненных записей: %d\n", rc);
						}
						else
						{
							wprintf_s(L"Ошибка. Не удалось сохранить записи в файл.\n");
						}
					}
					break;

					// загрузить из файла
					case CHAR_2:
					{
						int rc = students_load_from_file(&list, BACKUP_FILE);
						if (rc != -1)
						{
							wprintf_s(L"Записи успешно загружены из файла.\n");
							wprintf_s(L"Количество загруженных записей: %d\n", rc);
						}
						else
						{
							wprintf_s(L"Ошибка. Не удалось загрузить записи из файла.\n");
						}
					}
					break;

					default: break;
				}				
			}
			break;

			default: x = 0; break;
		}

		if (x == -1) { CLS; break; }

		if (x != 0) PAUSE;
	}
	

	return 0;
}