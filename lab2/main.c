﻿/*
Разреженный массив - массив, большинство элементов которого равны между собой, так что хранить в памяти достаточно лишь небольшое число значений отличных от основного (фонового) значения остальных элементов.

Разработать процедуры/функции, обеспечивающие доступ к элементам массива по номерам строки и столбца. В контрольной программе обеспечить запись и чтение всех элементов массива.

Элементы, значения которых являются фоновыми, называют нулевыми; элементы, значения которых отличны от фонового, - ненулевыми. Но нужно помнить, что фоновое значение не всегда равно нулю.
Ненулевые значения хранятся, как правило, в одномерном массиве, а связь между местоположением в исходном, разреженном, массиве и в новом, одномерном, описывается математически с помощью формулы, преобразующей индексы массива в индексы вектора.
На практике для работы с разреженным массивом разрабатываются функции:
•	а) для преобразования индексов массива в индекс вектора;
•	б) для получения значения элемента массива из ее упакованного представления по двум индексам (строка, столбец);
•	в) для записи значения элемента массива в ее упакованное представление по двум индексам.
При таком подходе обращение к элементам исходного массива выполняется с помощью указанных функций.

10. все нулевые элементы расположены в шахматном порядке, начиная с 1-го элемента 1-й строки
*/

#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


#define DEFAULT_VALUE 0
#define ELEMENT       int
#define ROWS          rows
#define COLS          cols


int ROWS = 0;
int COLS = 0;


int convert_index(int n, int m)
{
	if (n < 0 || m < 0) return -1;

	// считаем индекс нужного намэлемента в векторе

	// COLS - четное
	if (COLS % 2 == 0)
	{
		// n - четное, m - нечетное
		if (n % 2 == 0 && m % 2 != 0)
		{
			return (n * COLS / 2)  +  ((m - 1) / 2);
		}
		// n - нечетное, m - четное
		else if (n % 2 != 0 && m % 2 == 0)
		{
			return (n * COLS / 2) + (m / 2);
		}
	}
	// COLS - нечетное
	else
	{
		// n - четное, m - нечетное
		if (n % 2 == 0 && m % 2 != 0)
		{
			return (n * (COLS + 1) / 2) - (n / 2)  +  ((m - 1) / 2);
		}
		// n - нечетное, m - четное
		else if (n % 2 != 0 && m % 2 == 0)
		{
			return (n * (COLS + 1) / 2) - ((n + 1) / 2)  +  (m / 2);
		}
	}

	return -1;
}


int read_element(ELEMENT * arr, int n, int m)
{
	if (n < 0 || n >= ROWS || m < 0 || m >= COLS) return DEFAULT_VALUE;

	int index = convert_index(n, m);

	if (index < 0) return DEFAULT_VALUE;

	return (index < 0) ? DEFAULT_VALUE : arr[index];
}


_Bool write_element(ELEMENT * arr, int n, int m, ELEMENT value)
{
	if (n < 0 || n >= ROWS || m < 0 || m > COLS) return DEFAULT_VALUE;

	int index = convert_index(n, m);

	if (index < 0) return false;

	arr[index] = value;

	return true;
}


_Bool clean_stdin()
{
	while (getchar() != '\n');
	return true;
}


void get_integer(char * msg, int * v)
{
	char c;
	do
	{
		printf(msg);
	} while ((scanf("%d%c", v, &c) != 2 || c != '\n') && clean_stdin());
}


void print_matrix(int ** matrix)
{
	printf("\nSource matrix:\n");
	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLS; ++j)
		{
			printf("%d\t", matrix[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}


void print_vector(int * arr, int size)
{
	printf("\nVector:\n");
	for (int i = 0; i < size; ++i)
	{
		printf("%d\t", arr[i]);
	}
	printf("\n\n");
}



int main()
{
	// задаем размеры матрицы
	get_integer("Enter N: ", &ROWS);
	get_integer("Enter N: ", &COLS);

	if (ROWS < 1 || COLS < 1)
	{
		printf("wrong numbers");
		return 1;
	}

	// выделяем память для матрицы
	ELEMENT ** matrix = (ELEMENT **)malloc((ROWS)* sizeof(ELEMENT *));
	for (int i = 0; i < ROWS; i++) {
		matrix[i] = (ELEMENT *)malloc(COLS * sizeof(ELEMENT));
	}
	

#if 0
	matrix[0][0] = 0;
	matrix[0][1] = 8;
	matrix[0][2] = 0;
	matrix[0][3] = 6;
	matrix[0][4] = 0;

	matrix[1][0] = 1;
	matrix[1][1] = 0;
	matrix[1][2] = 2;
	matrix[1][3] = 0;
	matrix[1][4] = 4;

	matrix[2][0] = 0;
	matrix[2][1] = 7;
	matrix[2][2] = 0;
	matrix[2][3] = 2;
	matrix[2][4] = 0;

	matrix[3][0] = 5;
	matrix[3][1] = 0;
	matrix[3][2] = 7;
	matrix[3][3] = 0;
	matrix[3][4] = 9;

#endif


	// заполняем матрицу
#if 1

	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLS; ++j)
		{
			if ((i % 2 != 0 && j % 2 == 0) || (i % 2 == 0 && j % 2 != 0))
			{
				char msg[13];
				char buffer[10];
				strcpy(msg, "[");
				strcat(msg, itoa(i, buffer, 10));
				strcat(msg, ", ");
				strcat(msg, itoa(j, buffer, 10));
				strcat(msg, "] = ");

				get_integer(msg, &(matrix[i][j]));
			}
			else
			{
				matrix[i][j] = DEFAULT_VALUE;
			}
		}
	}

#endif
	

	// отображаем матрицу
	print_matrix(matrix);



	// считаем размер вектора
	int vector_size = 0;

	// ROWS - четное
	if (ROWS % 2 == 0) 
	{
		vector_size = ROWS / 2 * COLS;
	} 
	// ROWS - нечетное, COLS - четное
	else if (COLS % 2 == 0)
	{
		vector_size = (((ROWS + 1) / 2) * COLS) - (COLS / 2);
	}
	// ROWS - нечетное, COLS - нечетное 
	else
	{
		vector_size = (((ROWS + 1) / 2) * (COLS + 1)) - (((ROWS + 1) / 2)) - ((COLS + 1) / 2);
	}

	

	// выделяем память для вектора
	ELEMENT * arr = (ELEMENT *) malloc(vector_size * sizeof(ELEMENT));



	// заполняем вектор
	int k = 0;
	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLS; ++j)
		{
			if ((i % 2 != 0 && j % 2 == 0) || (i % 2 == 0 && j % 2 != 0))
			{
				arr[k] = matrix[i][j];
				++k;
			}
		}
	}



	// отображаем вектор
	print_vector(arr, vector_size);



	// читаем элемент
	printf("\nRead element\n");
	int  i = 0;
	int  j = 0;

	get_integer("Enter i (starts at 0): ", &i);
	get_integer("Enter j (starts at 0): ", &j);

	if (i < 0 || i >= ROWS || j < 0 || j >= COLS)
	{
		printf("wrong numbers\n");
	}
	else
	{
		printf("arr[%d, %d] = %d\n", i, j, read_element(arr, i, j));
	}
	


	// записываем элемент
	printf("\nWrite element\n");
	i = 0;
	j = 0;
	int  v = 0;

	get_integer("Enter i (starts at 0): ", &i);
	get_integer("Enter j (starts at 0): ", &j);
	get_integer("Enter new value: ", &v);

	if (i < 0 || i >= ROWS || j < 0 || j >= COLS)
	{
		printf("wrong numbers\n");
	}
	else
	{
		write_element(arr, i, j, v);
		print_vector(arr, vector_size);
	}


	return 0;
}